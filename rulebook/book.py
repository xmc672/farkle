# Copyright (C) 2020 Benjamin Böhme <mail[at]benjamin-boehme.net>
# Licensed under GNU GPLv3; see the README and the file "COPYING" for details.

""" Classes for creating several rulebook version """
class Section:
    def __init__(self):
        self.title = ''
        self.body = ''

class Book:
    def __init__(self):
        self.title = ''
        self.sections = []
