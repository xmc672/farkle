# Copyright (C) 2020 Benjamin Böhme <mail[at]benjamin-boehme.net>
# Licensed under GNU GPLv3; see the README and the file "COPYING" for details.

""" Strings that comprise the game's manual """
# unicode dice symbols (ud[i] = i pips)
ud = ['', u'\u2680', u'\u2681', u'\u2682', u'\u2683', u'\u2684', u'\u2685']

overview_title = 'Overview'
overview_raw = '''Farkle is a dice game for two or more players.
- Players agree on a certain winning score.
- Players take turns trying to add points to their bank score (details below).
- All players get the same amount of turns, i.e. some players might get a final turn even though another player already reached the winning score.
- The game ends if some player\'s bank score reached or exceeded the winning score, and everybody had the chance to play their final turn.
- The player with the highest bank score wins.'''
overview_wrap72 = '''Farkle is a dice game for two or more players.
  - Players agree on a certain winning score.
  - Players take turns trying to add points to their bank score
    (details below).
  - All players get the same amount of turns, i.e. some players might
    get a final turn even though another player already reached the
    winning score.
  - The game ends if some player\'s bank score reached or exceeded the
    winning score, and everybody had the chance to play their final turn.
  - The player with the highest bank score wins.'''

turns_title = 'Turns'
turns_raw = '''A player's turn starts with 6 dice and proceeds like this:

1)  Roll dice
2)  Farkle, i.e. no scorable combination?
        yes --> turn ends without banking points
        no  --> step 3
3)  Remove one or more scorable combinations of dice, points will be added to turn score
4)  Decide:
        roll remaining dice --> step 1
        end turn            --> step 5
5)  The accumulated turn score is added to the player's bank score.

Hot dice rule: if the player manages to remove and score all of the dice, the player will use all 6 dice for the next roll.'''
turns_wrap72 = '''A player's turn starts with 6 dice and proceeds like this:

  1)  Roll dice
  2)  Farkle, i.e. no scorable combination?
          yes --> turn ends without banking points
          no  --> step 3
  3)  Remove one or more scorable combinations of dice,
      points will be added to turn score
  4)  Decide:
          roll remaining dice --> step 1
          end turn            --> step 5
  5)  The accumulated turn score is added to the player's bank score.

Hot dice rule: if the player manages to remove and score all of the
dice, the player will use all 6 dice for the next roll.'''

scoring_title = 'Scoring'
scoring_raw = '''Scorable combinations of dice:
- single "1"s (100 points each)
- single "5"s (50 points each)
- three-of-a-kind, four-of-a-kind etc.

Points for three-of-a-kind:
    3 x "2" =  200 points
    3 x "3" =  300 points
    3 x "4" =  400 points
    3 x "5" =  500 points
    3 x "6" =  600 points
    3 x "1" = 1000 points

Four-of-a-kind give twice the amount of points of three-of-a-kind, five-of-a-kind give twice the amount of points of four-of-a-kind, etc.

Example:
    5 x "3" = 2 x (4 x "3")
            = 2 x 2 x (3 x "3")
            = 2 x 2 x 300
            = 1200'''
scoring_wrap72 = '''Scorable combinations of dice:
  - single "1"s (100 points each)
  - single "5"s (50 points each)
  - three-of-a-kind, four-of-a-kind etc.

Points for three-of-a-kind:
    3 x "2" =  200 points
    3 x "3" =  300 points
    3 x "4" =  400 points
    3 x "5" =  500 points
    3 x "6" =  600 points
    3 x "1" = 1000 points

Four-of-a-kind give twice the amount of points of three-of-a-kind,
five-of-a-kind give twice the amount of points of four-of-a-kind, etc.

Example:
    5 x "3" = 2 x (4 x "3")
            = 2 x 2 x (3 x "3")
            = 2 x 2 x 300
            = 1200'''
scoring_unicode = '''Scorable combinations of dice:
- single {}s (100 points each)
- single {}s (50 points each)
- three-of-a-kind, four-of-a-kind etc.

Points for three-of-a-kind:
    3 x {} =  200 points
    3 x {} =  300 points
    3 x {} =  400 points
    3 x {} =  500 points
    3 x {} =  600 points
    3 x {} = 1000 points

Four-of-a-kind give twice the amount of points of three-of-a-kind, five-of-a-kind give twice the amount of points of four-of-a-kind, etc.

Example:
    5 x {} = 2 x (4 x {})
            = 2 x 2 x (3 x {})
            = 2 x 2 x 300
            = 1200'''.format(ud[1], ud[5], ud[2], ud[3], ud[4], ud[5], ud[6], ud[1], ud[3], ud[3], ud[3])

example_title = 'Gameplay example'
example_raw = '''Here are two typical game situations:

Alice's roll:            (6, 6, 1, 2, 6, 6)
She selects for scoring:  ^  ^  ^     ^  ^

Points added to her turn score:
    1 x "1":   100
    4 x "6":  1200
    --------------
    total:    1300

Alice decides to pass and bank those points, as there would be a high risk of farkling in re-rolling the last remaining dice.

Bob's roll                (3, 4, 1, 1, 6, 5)
He selects for scoring:          ^

Bob would be able to score 250 points (2 x "1" = 200, 1 x "5" = 50), but he decides to only score a single "1" (=100 points) and re-roll the remaining five dice for a higher chance of rolling three-of-a-kind.

However, there is a risk that he will farkle and won't be able to bank those 100 points.'''
example_wrap72 = '''Here are two typical game situations:

Alice's roll:            (6, 6, 1, 2, 6, 6)
She selects for scoring:  ^  ^  ^     ^  ^

Points added to her turn score:
    1 x "1":   100
    4 x "6":  1200
    --------------
    total:    1300

Alice decides to pass and bank those points, as there would be a high
risk of farkling in re-rolling the last remaining dice.

Bob's roll                (3, 4, 1, 1, 6, 5)
He selects for scoring:          ^

Bob would be able to score 250 points (2 x "1" = 200, 1 x "5" = 50), but
he decides to only score a single "1" (=100 points) and re-roll the
remaining five dice for a higher chance of rolling three-of-a-kind.

However, there is a risk that he will farkle and won't be able to bank
those 100 points.'''
example_unicode = '''Here are two typical game situations:

Alice's roll:            {} {} {} {} {} {}
She selects for scoring all dice but the single {}.

Points added to her turn score:
    1 x {}:   100
    4 x {}:  1200
    --------------
    total:    1300

Alice decides to pass and bank those points, as there would be a high risk of farkling in re-rolling the last remaining dice.

Bob's roll                {} {} {} {} {} {}

Bob would be able to score 250 points (2 x {} = 200, 1 x {} = 50), but he decides to only score a single {} (=100 points) and re-roll the remaining five dice for a higher chance of rolling three-of-a-kind.

However, there is a risk that he will farkle and won't be able to bank those 100 points.'''.format(
    ud[6], ud[6], ud[1], ud[2], ud[6], ud[6],  ud[2],  ud[1], ud[6],  ud[3], ud[4], ud[1], ud[1], ud[6], ud[5],  ud[1], ud[5], ud[1])
