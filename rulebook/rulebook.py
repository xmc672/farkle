# Copyright (C) 2020 Benjamin Böhme <mail[at]benjamin-boehme.net>
# Licensed under GNU GPLv3; see the README and the file "COPYING" for details.

""" Several Farkle rulebooks (for monospace/GUI font) """
from rulebook import raw
from rulebook.book import Book, Section

def make_monospace_rulebook():
    book = Book()
    book.title = 'Farkle rulebook'
    book.sections = make_monospace_sections()
    return book

def make_monospace_sections():
    overview = Section()
    overview.title = raw.overview_title
    overview.body = raw.overview_wrap72
    turns = Section()
    turns.title = raw.turns_title
    turns.body = raw.turns_wrap72
    scoring = Section()
    scoring.title = raw.scoring_title
    scoring.body = raw.scoring_wrap72
    example = Section()
    example.title = raw.example_title
    example.body = raw.example_wrap72
    sections = [overview, turns, scoring, example]
    return sections

def make_varwidth_rulebook():
    book = Book()
    book.title = 'Farkle rulebook'
    book.sections = make_varwidth_sections()
    return book

def make_varwidth_sections():
    overview = Section()
    overview.title = raw.overview_title
    overview.body = raw.overview_raw
    turns = Section()
    turns.title = raw.turns_title
    turns.body = raw.turns_raw
    scoring = Section()
    scoring.title = raw.scoring_title
    scoring.body = raw.scoring_unicode
    example = Section()
    example.title = raw.example_title
    example.body = raw.example_unicode
    sections = [overview, turns, scoring, example]
    return sections

monospace = make_monospace_rulebook()
varwidth = make_varwidth_rulebook()
