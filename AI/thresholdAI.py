# Copyright (C) 2020 Benjamin Böhme <mail[at]benjamin-boehme.net>
# Licensed under GNU GPLv3; see the README and the file "COPYING" for details.

from game import eval

class ThresholdAI:
    """
    Simple farkle AI that tries to score a certain amount of points each turn
    and then passes.
    """
    def __init__(self, threshold):
        self._threshold = threshold

    def get_selection_and_decision(self, dice_values, turn_score):
        total_dice_score = eval.get_score_from_values(dice_values)
        max_new_score = turn_score + total_dice_score
        if max_new_score >= self._threshold:
            return self._score_all_and_pass_unless_hot_dice(dice_values)
        else:
            return self._score_minimal_and_continue(dice_values)

    def _score_all_and_pass_unless_hot_dice(self, dice_values):
        selection = eval.find_all_scoring_dice(dice_values)
        decision = len(selection) == len(dice_values)
        return selection, decision

    def _score_minimal_and_continue(self, dice_values):
        selection = eval.find_pips(1, dice_values)[0:1]
        if selection == []:
            selection = eval.find_pips(5, dice_values)[0:1]
        if selection == []:
            selection = eval.find_three_of_a_kind(dice_values)
        if selection == []:
            raise ValueError('Unable to select anything!')
        return selection, True
