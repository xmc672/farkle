# Copyright (C) 2020 Benjamin Böhme <mail[at]benjamin-boehme.net>
# Licensed under GNU GPLv3; see the README and the file "COPYING" for details.

""" Utilities for rolling dice """
from game import eval
import random

class Die:
    """ Emulates a single die """
    def __init__(self):
        self._top_face = 1

    @property
    def top_face(self):
        return self._top_face

    def __str__(self):
        return '{:d}'.format(self.top_face)

    def roll(self):
        self._top_face = random.randint(1, 6)

class Dice:
    """ Emulates a collection of dice that can be rolled simultaneously """
    def __init__(self, number_of_dice):
        self._list = []
        for i in range(number_of_dice):
            die = Die()
            self._list.append(die)
        self._score = eval.get_score_from_dice(self)

    @property
    def top_faces(self):
        return [die.top_face for die in self._list]

    @property
    def score(self):
        return self._score

    def __str__(self):
        return '{}'.format(self.top_faces)

    def roll(self):
        for die in self._list:
            die.roll()

    def remove(self, list_of_indices):
        """
        Removes dice specified by list of indices (0-based) and returns score of
        removed dice
        """
        list_of_indices.sort(reverse=True)
        removed_top_faces = []
        for i in list_of_indices:
            removed_top_faces.append(self._list[i].top_face)
            del self._list[i]
        removed_score = eval.get_score_from_values(removed_top_faces)
        return removed_score
