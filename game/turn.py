# Copyright (C) 2020 Benjamin Böhme <mail[at]benjamin-boehme.net>
# Licensed under GNU GPLv3; see the README and the file "COPYING" for details.

from enum import Enum
from game import eval
from game.dice import Dice

class TurnPhase(Enum):
    ROLL = 0
    SELECT = 1
    BANK = 2
    FARKLE = 3

class Turn:
    def __init__(self, player):
        self._is_over = False
        self._phase = TurnPhase.ROLL
        self._score = 0
        self._roll_score = 0
        self._current_player = player
        self._dice = Dice(6)

    @property
    def is_over(self):
        return self._is_over

    @property
    def phase(self):
        """ Returns a TurnPhase object. """
        return self._phase

    @property
    def score(self):
        """ Returns the accumulated turn score """
        return self._score

    @property
    def roll_score(self):
        return self._roll_score

    @property
    def current_player(self):
        return self._current_player

    @property
    def dice_values(self):
        """
        Returns the dice values of the current roll as a list of integers in
        the range 1-6.
        """
        return self._dice.top_faces

    def roll_dice(self):
        """
        Roll the remaining dice and update the turn phase as follows:
            farkled roll  -> FARKLE
            scorable roll -> SELECT
        """
        if not self.phase == TurnPhase.ROLL:
            raise RuntimeError('It is not the roll phase!')
        if self.dice_values == []:
            self._dice = Dice(6)
        self._dice.roll()
        if eval.is_farkle(self._dice):
            self._phase = TurnPhase.FARKLE
            self._score = 0
            self._roll_score = 0
        else:
            self._phase = TurnPhase.SELECT

    def score_dice(self, selection):
        """
        Score the selected dice.
        'Selection' must be a non-empty list only containing numbers in the
        range 0-5 which specify the selected dice.
        """
        if self.phase != TurnPhase.SELECT:
            raise RuntimeError('It is not the select phase!')
        self._validate_selection(selection)
        self._roll_score = self._dice.remove(selection)
        self._score += self.roll_score
        self._phase = TurnPhase.ROLL

    def _validate_selection(self, selection):
        if not selection:
            raise ValueError('Selection must not be empty!')
        selected_pips = [self.dice_values[i] for i in selection]
        for pips in [2, 3, 4, 6]:
            if 1 <= selected_pips.count(pips) <= 2:
                msg = 'Selection must contain either no or at least three {}\'s!'
                raise ValueError( msg.format(pips))

    def start_banking_phase(self):
        """
        Set the turn phase to BANKING.
        Call this when the current player decides not to re-roll the remaining
        dice, but to bank their points and end their turn instead.
        """
        if self.phase != TurnPhase.ROLL:
            raise RuntimeError('It is not the select phase!')
        self._phase = TurnPhase.BANK

    def _bank_score_and_end_turn(self):
        self._current_player.increase_bank_score(self.score)
        self._is_over = True
