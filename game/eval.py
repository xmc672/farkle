# Copyright (C) 2020 Benjamin Böhme <mail[at]benjamin-boehme.net>
# Licensed under GNU GPLv3; see the README and the file "COPYING" for details.

""" Utilities for evaluating dice rolls """

def find_pips(pips, dice_values):
    """ Return all the indices of dice showing given pips."""
    return [i for (i, v) in enumerate(dice_values) if v == pips]

def find_three_of_a_kind(dice_values):
    """Return indices of highest-scoring three-of-a-kind combo."""
    for pips in [1, 6, 5, 4, 3, 2]:
        if dice_values.count(pips) >= 3:
            indices = find_pips(pips, dice_values)
            return indices[0:3]
    raise ValueError('There aren\'t three of any kind!')

def find_all_scoring_dice(dice_values):
    """Return indices of all scoring dice, in no particular order."""
    indices = []
    for pips in [1, 5]:
        indices += find_pips(pips, dice_values)
    for pips in [2, 3, 4, 6]:
        if dice_values.count(pips) >= 3:
            indices += find_pips(pips, dice_values)
    return indices

def is_valid_selection(dice_values):
    if dice_values == []:
        return False
    for pips in [2, 3, 4, 6]:
        if 0 < dice_values.count(pips) < 3:
            return False
    return True

def is_farkle(dice):
    return get_score_from_dice(dice) == 0

def get_score_from_dice(dice):
    """ Returns the score of a Dice instance """
    values = dice.top_faces
    return get_score_from_values(values)

def get_score_from_values(values):
    """ Returns the score of a list of dice values 1-6 """
    dict = {}
    for pips in range(1, 7):
        dict[pips] = values.count(pips)
    return _get_score_from_dict(dict)

def _get_score_from_dict(dict):
    return _score_triplets(dict) + _score_one(dict) + _score_five(dict)

def _score_triplets(dict):
    sum = 0
    for pips in range(1, 7):
        sum += _score_triplet(dict, pips)
    return sum

def _score_triplet(dict, pips):
    pip_factor = pips * 100 if pips != 1 else 1000
    count = dict[pips]
    count_factor = 0
    if count >= 3:
        count_factor = 2 ** (count - 3)
    return count_factor * pip_factor

def _score_one(dict):
    count = dict[1]
    if count < 3:
        return count * 100
    return 0

def _score_five(dict):
    count = dict[5]
    if count < 3:
        return count * 50
    return 0
