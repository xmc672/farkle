# Copyright (C) 2020 Benjamin Böhme <mail[at]benjamin-boehme.net>
# Licensed under GNU GPLv3; see the README and the file "COPYING" for details.

from game.player import Player

class PlayerLoop:
    """
    Allows to loop through a list of players and get the current one, a list
    of all of them, or the one with the highest score.
    """
    def __init__(self, players_are_human):
        """
        players_are_human: list of Booleans where True stands for a human player
        and False stands for an AI player
        """
        self._index = 0
        self._init_players(players_are_human)

    def _init_players(self, players_are_human):
        self._player_list = []
        for i, bool in enumerate(players_are_human):
            self._player_list.append(Player(i+1, 'Player {}'.format(i+1), is_human=bool))

    @property
    def current_player(self):
        return self._player_list[self._index]

    @property
    def highest_scoring_player(self):
        players_sorted_by_bank_score = sorted(
            self._player_list,
            key=lambda p: p.bank_score)
        return players_sorted_by_bank_score[-1]

    @property
    def at_end_of_loop(self):
        return self._index == len(self._player_list) - 1

    def list_players(self):
        # better to return a copy???
        return self._player_list

    def loop(self):
        """
        Let current_player point to the next player in the list, in a
        circular fashion.
        """
        max = len(self._player_list)
        if self._index < max - 1:
            self._index += 1
        else:
            self._index = 0
