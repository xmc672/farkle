# Copyright (C) 2020 Benjamin Böhme <mail[at]benjamin-boehme.net>
# Licensed under GNU GPLv3; see the README and the file "COPYING" for details.

from game.playerloop import PlayerLoop
from game.turn import Turn

class GameSettings:
    """ Struct with data needed to initialize game instance """
    def __init__(self, winning_score=6000, players_are_human=[True, False]):
        self.winning_score = winning_score
        self.players_are_human = players_are_human

class Game:
    def __init__(self, game_settings):
        """
        game_settings: a GameSettings instance
        """
        self._winning_score = game_settings.winning_score
        self._is_over = False
        self._is_last_round = False
        self._players = PlayerLoop(game_settings.players_are_human)
        self._winner = None
        self._turn = Turn(self._players.current_player)

    @property
    def winning_score(self):
        return self._winning_score

    @property
    def is_over(self):
        return self._is_over

    @property
    def is_last_round(self):
        return self._is_last_round

    @property
    def player_list(self):
        return self._players.list_players()

    @property
    def current_player(self):
        return self._players.current_player

    @property
    def winner(self):
        return self._winner

    @property
    def turn(self):
        """ Returns the current Turn instance """
        return self._turn

    @property
    def dice_values(self):
        """
        Returns the dice values of the current roll as a list of integers in
        the range 1-6.
        """
        return self._turn.dice_values

    def update(self):
        if self.is_over:
            raise RuntimeError('Game is already over!')
        if not self._turn.is_over:
            raise RuntimeError('Turn is not over yet!')
        if self._current_player_triggers_last_round():
            self._is_last_round = True
        if self._is_last_round and self._players.at_end_of_loop:
            self._end_game()
        else:
            self._make_next_turn()

    def _current_player_triggers_last_round(self):
        return self._players.current_player.bank_score >= self._winning_score

    def _end_game(self):
        self._winner = self._players.highest_scoring_player
        self._is_over = True

    def _make_next_turn(self):
        self._players.loop()
        self._turn = Turn(self._players.current_player)

    def score_dice(self, selection):
        """
        Scores the current player's selection, passed as a list of dice indices
        (0-based).
        """
        self._turn.score_dice(selection)
        self.update()
