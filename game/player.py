# Copyright (C) 2020 Benjamin Böhme <mail[at]benjamin-boehme.net>
# Licensed under GNU GPLv3; see the README and the file "COPYING" for details.

class Player:
    def __init__(self, id, name, is_human=True):
        self._id = id
        self._name = name
        self._is_human = is_human
        self._bank_score = 0

    @property
    def id(self):
        return self._id

    @property
    def name(self):
        return self._name

    @property
    def is_human(self):
        return self._is_human

    @property
    def bank_score(self):
        return self._bank_score

    def increase_bank_score(self, amount):
        self._bank_score += amount
