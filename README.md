## Farkle v0.2
This is a Python implementation of the popular dice game "Farkle".
Two players (human or computer players) can play Farkle against each other on
the same machine. The app has both a graphical UI and a console UI.

Copyright (C) 2020  Benjamin Böhme <mail[at]benjamin-boehme.net>

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program.  If not, see <https://www.gnu.org/licenses/>.

[Dependencies](#dependencies)  
[Run the app](#run-the-app)  
[Rules of the game](#rules-of-the-game)  
[Change log](#change-log)  

### Dependencies
You need *Python3* to run the app. It's tested for *Python 3.5*, but some
older versions will probably work, too.

In addition, you need *TkInter* to run the GUI. The app is tested for *TkInter 8.6*.
On a Debian-like system, one of the following commands should install the required package:

    sudo apt-get install python-tk

or

    sudo apt-get install python3-tk


### Run the app
Run

    python3 main.py

to start the GUI, or

    python3 main.py --console

to start the console interface (e.g. if you have trouble with *TkInter*).

### Rules of the game
Both the graphical and the console UI have a built-in rulebook. See there if
you're not a familiar with the rules of the game.

### Change log
Version 0.2:
- added graphical UI
- improved rulebook text
