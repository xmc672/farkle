# Copyright (C) 2020 Benjamin Böhme <mail[at]benjamin-boehme.net>
# Licensed under GNU GPLv3; see the README and the file "COPYING" for details.

"""
Either run GUI (default) or console UI (command line option -c/--console.
"""
import argparse

parser = argparse.ArgumentParser()
parser.add_argument(
    '-c',
    '--console',
    action='store_true',
    help='use console UI instead of graphical UI')
args = parser.parse_args()

if args.console:
    from simpleUI import simpleUI
else:
    from tkGUI import TkGUI
