# Copyright (C) 2020 Benjamin Böhme <mail[at]benjamin-boehme.net>
# Licensed under GNU GPLv3; see the README and the file "COPYING" for details.

"""
A simple shared console UI for two human players
"""
from AI.thresholdAI import ThresholdAI
from game.game import Game, GameSettings
from game.turn import TurnPhase
from simpleUI import console

def setup():
    global winning_score, players_are_human, ai
    winning_score = 6000
    players_are_human = [True, False]
    ai = ThresholdAI(350)

def main_menu():
    while True:
        console.main_menu_message()
        menu_choice = _get_menu_item()
        if menu_choice == 1:
            console.show_rules()
        elif menu_choice == 2:
            _play_game()
        else:
            console.shutdown_message()
            exit()

def _get_menu_item():
    return console.get_player_choice(
        'Choose an option:',
        ['Read the rules of the game',
        'Start a new game of Farkle',
        'Quit'])

def _play_game():
    _handle_setup()
    while not game.is_over:
        _handle_turn(game.turn)
        game.update()
    console.end_of_game_message(game.winner.name)

def _handle_setup():
    global game, winning_score, players_are_human
    _print_game_settings()
    if console.get_player_y_or_n('Change game settings?'):
        winning_score = _get_winning_score()
        players_are_human = _get_player_settings()
    game_settings = GameSettings(
        winning_score=winning_score,
        players_are_human=players_are_human)
    game = Game(game_settings)

def _print_game_settings():
    print('Game settings:')
    print('  Winning score:     {}'.format(winning_score))
    print('  Player 1 is human: {}'.format(players_are_human[0]))
    print('  Player 2 is human: {}'.format(players_are_human[1]))

def _get_winning_score():
    choice = console.get_player_choice('Choose winning score:',
                              ['Short game  ( 2000 points)',
                               'Medium game ( 6000 points)',
                               'Long game   (10000 points)'])
    dict = {1: 2000, 2: 6000, 3: 10000}
    return dict[choice]

def _get_player_settings():
    p1_human = console.get_player_y_or_n('Is Player1 human?')
    p2_human = console.get_player_y_or_n('Is Player2 human?')
    return [p1_human, p2_human]

def _handle_turn(turn):
    console.new_turn_message(turn.current_player.name, game.is_last_round)
    while not turn.is_over:
        if turn.phase == TurnPhase.ROLL:
            _handle_roll(turn)
        elif turn.phase == TurnPhase.SELECT:
            _handle_selection(turn)
        elif turn.phase == TurnPhase.BANK:
            _handle_banking(turn)
        elif turn.phase == TurnPhase.FARKLE:
            _handle_farkle(turn)
    console.get_player_ack()
    console.print_score(game.player_list, game.winning_score)
    console.get_player_ack()

def _handle_roll(turn):
    print('Your accumulated turn score is {}.\n'.format(turn.score))
    print('Rolling dice...\n')
    turn.roll_dice()
    console.print_dice(turn.dice_values)

def _handle_selection(turn):
    if game.current_player.is_human:
        _handle_human_selection(turn)
    else:
        _handle_AI_selection(turn)

def _handle_human_selection(turn):
    max_index = len(turn.dice_values)
    while True:
        selection = console.get_player_selection(max_index)
        try:
            turn.score_dice(selection)
        except ValueError:
            print('''Invalid selection!
For pips 2, 3, 4, 6, you must choose either no or least three of a kind!
You must choose at least one die!
''')
        else:
            print('You scored {} points.'.format(turn.roll_score))
            break
    if not console.get_player_y_or_n('Continue turn?'):
        turn.start_banking_phase()

def _handle_AI_selection(turn):
    selection, continue_flag = ai.get_selection_and_decision(
        turn.dice_values, turn.score)
    selection_string = ''.join([str(i+1) for i in selection])
    print('The computer selected the dice {}.\n'.format(selection_string))
    turn.score_dice(selection)
    print('The computer scored {} points.'.format(turn.roll_score))
    console.get_player_ack()
    if not continue_flag:
        turn.start_banking_phase()

def _handle_banking(turn):
    console.banking_message(turn.current_player.name, turn.score)
    turn._bank_score_and_end_turn()

def _handle_farkle(turn):
    console.farkle_message(turn.current_player.name)
    turn._bank_score_and_end_turn()

# executable code
setup()
console.startup_message()
main_menu()
