# Copyright (C) 2020 Benjamin Böhme <mail[at]benjamin-boehme.net>
# Licensed under GNU GPLv3; see the README and the file "COPYING" for details.

"""
Tools and utilities for console input and output for Farkle.
"""
from rulebook import rulebook

def startup_message():
    print('''===== Farkle v0.2 =====
Copyright (C) 2020  Benjamin Böhme  mail[at]benjamin-boehme.net

This program comes with ABSOLUTELY NO WARRANTY. This is free software,
and you are welcome to redistribute it under certain conditions. See
https://www.gnu.org/licenses/gpl-3.0 for details.

''')

def main_menu_message():
    print('=== Farkle main menu ===\n')

def show_rules():
    for section in rulebook.monospace.sections:
        print('=== {} ==='.format(section.title))
        print(section.body + '\n')
        get_player_ack()
    print('\n')

def new_turn_message(player_name, is_final_turn):
    last_round_string = 'final ' if is_final_turn else ''
    print('+++ It\'s {}\'s {}turn +++'.format(player_name, last_round_string))

def banking_message(player_name, turn_score):
    print('{} banks {} points.'.format(player_name, turn_score))

def farkle_message(player_name):
    print('Farkle! {}\'s turn ends and the turn score is lost!'.format(player_name))

def end_of_game_message(winner_name):
    print('The game is over. {} wins!'.format(winner_name))
    get_player_ack()
    print('\n')

def shutdown_message():
    print('Goodbye!')

def print_score(player_list, winning_score):
    width = len(str(winning_score))
    print('          Bank score'.format(winning_score))
    for player in player_list:
        print('{:>8}: {:>{w}}/{}'.format(
            player.name, player.bank_score, winning_score, w=width))
    print('')

def print_dice(dice_values):
    n = len(dice_values)
    number_string = ' '.join([str(i) for i in range(1, n+1)])
    dice_string = ' '.join([str(v) for v in dice_values])
    print('Die no.: {}'.format(number_string))
    print('   Pips: {}'.format(dice_string))
    print('')

def get_player_selection(max_index):
    prompt = 'Enter the numbers of the dice you\'d like to score!\n'
    warning = 'Invalid input!\n'
    example = 'Example: type 136 to select the dice 1, 3 and 6\n'
    while True:
        input_string = input(prompt)
        try:
            selection = _string_to_index_list(input_string, max_index)
        except ValueError:
            print(warning + example)
        else:
            return selection

def _string_to_index_list(s, max_index):
    if not s.isdigit():
        raise ValueError('String must only contain digits!')
    l = [int(c) for c in list(s)]
    for i in l:
        if not 1 <= i <= max_index:
            raise ValueError('Digits must be in the range 1-{}!'.format(max_index))
    for index in range(1, max_index):
        if l.count(index) > 1:
            raise ValueError('There must not be duplicate entries!')
    l = [i - 1 for i in l] # 1-based --> 0-based
    return l

def get_player_ack():
    prompt = 'Press <Return> to continue\n'
    input(prompt)

def get_player_y_or_n(message):
    while True:
        input_string = input('{} (yes=y/1, no=n/0)\n'.format(message))
        if input_string in ['y', '1']:
            return True
        elif input_string in ['n', '0']:
            return False
        else:
            print('Invalid input! Enter \'y\', \'n\', \'1\' or \'0\'!\n')

def get_player_choice(message, choice_strings):
    """
    Let user choose between different options from the console.
        message: general explanation/question
        choice_strings: list of strings explaining each choice
    """
    while True:
        print('{}\n'.format(message))
        number_of_choices = len(choice_strings)
        for i, choice_string in enumerate(choice_strings):
            print('{}: {}'.format(i+1, choice_string))
        input_string = input('\n')
        if input_string in [str(i+1) for i in range(number_of_choices)]:
            return int(input_string)
        else:
            print('Invalid input! Enter a number in the range 1-{}!\n'.format(
                number_of_choices))
