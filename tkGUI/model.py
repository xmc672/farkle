# Copyright (C) 2020 Benjamin Böhme <mail[at]benjamin-boehme.net>
# Licensed under GNU GPLv3; see the README and the file "COPYING" for details.

from AI.thresholdAI import ThresholdAI
from game.game import GameSettings

class Model:
    def __init__(self):
        self.game_settings = GameSettings()
        self.game = None
        self.selected_indices = [] # indices of selected dice, 0-based
        self.ai = ThresholdAI(350)
