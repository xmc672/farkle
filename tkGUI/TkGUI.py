# Copyright (C) 2020 Benjamin Böhme <mail[at]benjamin-boehme.net>
# Licensed under GNU GPLv3; see the README and the file "COPYING" for details.

from tkinter import Tk
root = Tk()

from tkGUI.controller.controller import Controller
app = Controller(root)
app.mainloop()
