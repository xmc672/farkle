# Copyright (C) 2020 Benjamin Böhme <mail[at]benjamin-boehme.net>
# Licensed under GNU GPLv3; see the README and the file "COPYING" for details.

import logging
from tkinter import END

class ScrolledTextHandler(logging.Handler):
    """
    Logging Handler implementation that can write to a given tkinter
    ScrolledText widget
    """
    def __init__(self, scrolled_text_widget):
        logging.Handler.__init__(self, level=logging.INFO)
        self.scrolled_text_widget = scrolled_text_widget

    def emit(self, record):
        log_entry = self.format(record)
        def append():
            self.scrolled_text_widget.config(state='normal')
            self.scrolled_text_widget.insert(END, log_entry + '\n')
            self.scrolled_text_widget.config(state='disabled')
            self.scrolled_text_widget.yview(END)
        self.scrolled_text_widget.after(100, append)

def get_logger(scrolled_text_widget, level=logging.INFO):
    logger = logging.getLogger()
    logger.addHandler(ScrolledTextHandler(scrolled_text_widget))
    logger.setLevel(level)
    return logger
