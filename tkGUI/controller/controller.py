# Copyright (C) 2020 Benjamin Böhme <mail[at]benjamin-boehme.net>
# Licensed under GNU GPLv3; see the README and the file "COPYING" for details.

from game import eval
from game.game import Game
from game.turn import TurnPhase
from tkGUI.controller import logtools
from tkGUI.model import Model
from tkGUI.view.mainwindow import MainWindow
from tkGUI.view.menubar import MenuBar
from tkGUI.view.startgamewindow import StartGameWindow
from tkinter.constants import NORMAL

class Controller:
    def __init__(self, root):
        self.root = root
        self.root.title('Farkle')
        self.model = Model()
        self._init_main_view()
        self._init_logger()

    def _init_main_view(self):
        self.main_view = MainWindow()
        self.menubar = MenuBar(self.root)
        self.menubar.game_menu.entryconfig(0, command=self._make_newgame_view)
        self.root.config(menu=self.menubar)

    def _init_logger(self):
        self.logger = logtools.get_logger(self.main_view.log_frame.scrolledtext)

    def mainloop(self):
        self.root.mainloop()

    def _make_newgame_view(self):
        self.newgame_view = StartGameWindow()
        self.newgame_view.winning_score_slider.var.set(self.model.game_settings.winning_score)
        self.newgame_view.player1_checkbutton.var.set(self.model.game_settings.players_are_human[0])
        self.newgame_view.player2_checkbutton.var.set(self.model.game_settings.players_are_human[1])
        self.newgame_view.start_button.config(command=self._start_new_game)

    def _start_new_game(self):
        self._apply_game_settings_from_newgame_view()
        self.newgame_view.destroy()
        self._log_new_game()
        self.model.game = Game(self.model.game_settings)
        self.model.selected_indices = []
        self._reset_right_pane()
        self._make_keybindings()
        self._handle_game()

    def _apply_game_settings_from_newgame_view(self):
        self.model.game_settings.winning_score = self.newgame_view.winning_score_slider.var.get()
        self.model.game_settings.players_are_human[0] = self.newgame_view.player1_checkbutton.var.get()
        self.model.game_settings.players_are_human[1] = self.newgame_view.player2_checkbutton.var.get()

    def _log_new_game(self):
        self.logger.info('Starting a new game...')
        self.logger.info('- Winning score: {}'.format(self.model.game_settings.winning_score))
        self.logger.info('- Player1 is human: {}'.format(self.model.game_settings.players_are_human[0]))
        self.logger.info('- Player2 is human: {}'.format(self.model.game_settings.players_are_human[1]))

    def _reset_right_pane(self):
        self.main_view.reset_right_pane()
        self.main_view.player_frame.make_player_labels(self.model.game.player_list)
        self._update_player_frame()
        self.main_view.game_frame.dice_frame.selection_changed_callback = self._dice_selection_changed
        self.main_view.game_frame.button_frame.pass_button.config(command=self._pass_button_invoked)
        self.main_view.game_frame.button_frame.reroll_button.config(command=self._reroll_button_invoked)
        self.main_view.game_frame.button_frame.continue_button.config(command=self._continue_button_invoked)

    def _update_player_frame(self):
        self.main_view.player_frame.update_player_labels(
            self.model.game.player_list,
            self.model.game.current_player,
            self.model.game.winning_score)

    def _get_player_info_string(self):
        winning_score = self.model.game.winning_score
        string_list = []
        for player in self.model.game.player_list:
            s = '{}: {}/{}'.format(player.name, player.bank_score, winning_score)
            string_list.append(s)
        return '\n'.join(string_list)

    def _make_keybindings(self):
        for i in range(6):
            self.root.bind('{}'.format(i+1), lambda e, i=i: self._number_key_pressed(i))
            self.root.bind('<KP_{}>'.format(i+1), lambda e, i=i: self._number_key_pressed(i))
        self.root.bind('<Return>', lambda e: self._return_key_pressed())
        self.root.bind('<KP_Enter>', lambda e: self._return_key_pressed())
        self.root.bind('<space>', lambda e: self._space_key_pressed())
        self.root.bind('<KP_0>', lambda e: self._space_key_pressed())

    ################################################################################
    # handle game, turn, end-of-game
    def _handle_game(self):
        if self.model.game.turn.is_over:
            self.model.game.update()
            self._update_player_frame()
        if self.model.game.is_over:
            self._handle_end_of_game()
        else:
            self._handle_turn()

    def _handle_end_of_game(self):
        msg = 'The game is over. {} wins!'.format(self.model.game.winner.name)
        self.logger.info(msg)
        self.main_view.game_frame.update_msg(msg)
        self.main_view.game_frame.hide_turn_score()
        self.main_view.game_frame.dice_frame._disable_dice()
        self.main_view.game_frame.button_frame.hide_action_buttons()
        self.main_view.game_frame.button_frame.hide_continue_button()

    def _handle_turn(self):
        switch = {
            TurnPhase.ROLL: self._handle_roll,
            TurnPhase.FARKLE: self._handle_farkle,
            TurnPhase.SELECT: self._handle_selection,
            TurnPhase.BANK: self._handle_banking
            }
        switch[self.model.game.turn.phase]()

    ################################################################################
    # handle ROLL phase
    def _handle_roll(self):
        self._update_turn_score()
        self.main_view.game_frame.update_msg('Rolling dice...')
        self.main_view.game_frame.show_turn_score()
        self.main_view.game_frame.show_selected_score()
        self.model.game.turn.roll_dice()
        self._log_roll()
        self._update_dice()
        self._handle_turn()

    def _update_turn_score(self):
        return self.main_view.game_frame.update_turn_score(self.model.game.turn.score)

    def _log_roll(self):
        return self.logger.info('{} rolls {}'.format(
            self.model.game.turn.current_player.name,
            self.model.game.turn.dice_values))

    def _update_dice(self):
        return self.main_view.game_frame.dice_frame._update_dice(
            self.model.game.turn.dice_values,
            selected_indices=self.model.selected_indices)

    ################################################################################
    # handle FARKLE phase
    def _handle_farkle(self):
        self.logger.info('{} farkles!'.format(self.model.game.current_player.name))
        self.main_view.game_frame.update_msg('Farkle! The turn ends and the turn score is lost!')
        self.main_view.game_frame.dice_frame._disable_dice()
        self.main_view.game_frame.hide_selected_score()
        self._update_button_frame_for_farkle()
        self.model.game.turn._bank_score_and_end_turn()

    def _update_button_frame_for_farkle(self):
        self.main_view.game_frame.button_frame.hide_action_buttons()
        self.main_view.game_frame.button_frame.show_continue_button()

    ################################################################################
    # handle SELECTION phase
    def _handle_selection(self):
        if self.model.game.current_player.is_human:
            self._handle_human_selection()
        else:
            self._handle_AI_selection()

    def _handle_human_selection(self):
        final_round_msg = 'Final round! ' if self.model.game.is_last_round else ''
        self.main_view.game_frame.update_msg('{}{}, make your move!'.format(
            final_round_msg,
            self.model.game.current_player.name))
        self._update_selected_score()
        self._update_button_frame_for_selection()

    def _update_selected_score(self):
        selected_dice_values = self._get_selected_dice_values()
        if selected_dice_values == []:
            self.main_view.game_frame.hide_selected_score()
        elif not eval.is_valid_selection(selected_dice_values):
            self.main_view.game_frame.update_selected_score(0, invalid=True)
            self.main_view.game_frame.show_selected_score()
        else:
            score = eval.get_score_from_values(selected_dice_values)
            self.main_view.game_frame.update_selected_score(score)
            self.main_view.game_frame.show_selected_score()

    def _get_selected_dice_values(self):
        dice_values = []
        for i in self.model.selected_indices:
            dice_values.append(self.model.game.turn.dice_values[i])
        return dice_values

    def _update_button_frame_for_selection(self):
        self.main_view.game_frame.button_frame.hide_continue_button()
        self.main_view.game_frame.button_frame.show_action_buttons()
        self.main_view.game_frame.button_frame.disable_action_buttons()

    def _handle_AI_selection(self):
        selection, continue_flag = self.model.ai.get_selection_and_decision(
            self.model.game.dice_values,
            self.model.game.turn.score)
        self.model.selected_indices = selection
        self._update_dice()
        self.main_view.game_frame.dice_frame._disable_dice()
        self.model.game.turn.score_dice(selection)
        self.model.selected_indices = []
        final_round_msg = 'Final round! ' if self.model.game.is_last_round else ''
        scoring_msg = '{} scores {} points.'.format(
            self.model.game.current_player.name,
            self.model.game.turn.roll_score)
        self.main_view.game_frame.update_msg('{}{}'.format(final_round_msg, scoring_msg))
        self.logger.info(scoring_msg)
        self._update_turn_score()
        self.main_view.game_frame.hide_selected_score()
        self.main_view.game_frame.button_frame.hide_action_buttons()
        self.main_view.game_frame.button_frame.show_continue_button()
        if not continue_flag:
            self.model.game.turn.start_banking_phase()

    ################################################################################
    # handle BANKING phase
    def _handle_banking(self):
        msg = '{} banks {} points.'.format(
            self.model.game.current_player.name,
            self.model.game.turn.score)
        self.model.game.turn._bank_score_and_end_turn()
        self._update_player_frame()
        self.main_view.game_frame.update_msg(msg)
        self.main_view.game_frame.hide_turn_score()
        self.main_view.game_frame.hide_selected_score()
        self.logger.info(msg)

    ################################################################################
    # button callbacks
    def _number_key_pressed(self, number):
        try:
            button = self.main_view.game_frame.dice_frame.dice_buttons[number]
        except IndexError:
            return
        if button.cget('state') == NORMAL:
            self._dice_selection_changed(number)

    def _return_key_pressed(self):
        reroll_button = self.main_view.game_frame.button_frame.reroll_button
        continue_button = self.main_view.game_frame.button_frame.continue_button
        for button in [reroll_button, continue_button]:
            if button.winfo_viewable():
                button.invoke()

    def _space_key_pressed(self):
        pass_button = self.main_view.game_frame.button_frame.pass_button
        continue_button = self.main_view.game_frame.button_frame.continue_button
        for button in [pass_button, continue_button]:
            if button.winfo_viewable():
                button.invoke()

    def _dice_selection_changed(self, die_index):
        self._toggle_die_button(die_index)
        self._update_selected_indices(die_index)
        self._update_selected_score()
        self._update_action_buttons_upon_selection()

    def _toggle_die_button(self, die_index):
        toggled_die_button = self.main_view.game_frame.dice_frame.dice_buttons[die_index]
        if die_index in self.model.selected_indices:
            toggled_die_button.unselect()
        else:
            toggled_die_button.select()

    def _update_selected_indices(self, die_index):
        """ Add die_index if not contained, remove if already contained """
        selection = self.model.selected_indices
        if die_index in selection:
            selection.remove(die_index)
        else:
            selection.append(die_index)
            selection.sort()

    def _update_action_buttons_upon_selection(self):
        dice_values = self._get_selected_dice_values()
        if dice_values == []:
            self.main_view.game_frame.button_frame.disable_action_buttons()
        elif eval.is_valid_selection(dice_values):
            self.main_view.game_frame.button_frame.enable_action_buttons()
        else:
            self.main_view.game_frame.button_frame.disable_action_buttons()

    def _pass_button_invoked(self):
        self._action_button_invoked()
        self.model.game.turn.start_banking_phase()

    def _reroll_button_invoked(self):
        self._action_button_invoked()

    def _action_button_invoked(self):
        self.model.game.turn.score_dice(self.model.selected_indices)
        self.model.selected_indices = []
        scoring_msg = '{} scores {} points.'.format(
            self.model.game.current_player.name,
            self.model.game.turn.roll_score)
        self.logger.info(scoring_msg)
        self.main_view.game_frame.update_msg(scoring_msg)
        self._update_turn_score()
        self.main_view.game_frame.dice_frame._disable_dice()
        self._update_selected_score()
        self.main_view.game_frame.button_frame.hide_action_buttons()
        self.main_view.game_frame.button_frame.show_continue_button()

    def _continue_button_invoked(self):
        self._handle_game()
