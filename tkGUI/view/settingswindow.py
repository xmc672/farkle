# Copyright (C) 2020 Benjamin Böhme <mail[at]benjamin-boehme.net>
# Licensed under GNU GPLv3; see the README and the file "COPYING" for details.

from tkGUI.view.guisettings import gui_settings
from tkinter import Toplevel, Frame, Scale, Label
from tkinter.constants import HORIZONTAL

class SettingsWindow(Toplevel):
    def __init__(self):
        super().__init__()
        self.title('Settings')
        self._make_font_size_frame()

    def _make_font_size_frame(self):
        font_size_frame = Frame(self)
        font_size_label = Label(
            font_size_frame,
            font=gui_settings.fonts['menu'],
            text='Appearance')
        font_size_label.pack()
        font_size_frame.pack()
        self._make_relative_font_size_slider(font_size_frame)

    def _make_relative_font_size_slider(self, font_size_frame):
        slider = Scale(
            font_size_frame,
            length=150,
            from_=-3,
            to=3,
            orient=HORIZONTAL,
            font=gui_settings.fonts['small caption'],
            label='Relative font size',
            command=lambda x: gui_settings.adjust_nondice_font_sizes(int(x)))
        initial_relative_change = 0
        slider.set(initial_relative_change)
        slider.pack()
