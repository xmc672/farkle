# Copyright (C) 2020 Benjamin Böhme <mail[at]benjamin-boehme.net>
# Licensed under GNU GPLv3; see the README and the file "COPYING" for details.

from tkGUI.view.guisettings import gui_settings, default_bg_color
from tkinter import Toplevel
from tkinter.constants import BOTH
from tkinter.ttk import Treeview, Style

class KeyWindow(Toplevel):
    """ Window with information about keybindings """
    def __init__(self):
        super().__init__()
        self.title('Keybindings')
        self._make_table()

    def _make_table(self):
        style = Style()
        style.configure(
            'Treeview',
            rowheight=30,
            background=default_bg_color,
            fieldbackground=default_bg_color)
        self.table = Treeview(
            self,
            selectmode='none',
            show='tree',
            columns=('Key(s)'))
        self.table.column('#0', minwidth=200, width=200)
        self.table.column('Key(s)', minwidth=300, width=300)
        self.table.pack(fill=BOTH, expand=1)
        self.table.tag_configure('std', font=gui_settings.fonts['default'])
        for command_string, key_string in keybinding_dict.items():
            self.table.insert(
                '',
                0,
                text=command_string,
                values=[key_string],
                tags=('std'))

keybinding_dict = {
    'Select/unselect dice': 'Number keys 1-6 (also NumPad)',
    'Score and continue': '<Return> (also NumPad)',
    'Score and pass': '<Space> or NumPad 0',
    'Continue': '<Return> or <Space>'
    }
