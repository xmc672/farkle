# Copyright (C) 2020 Benjamin Böhme <mail[at]benjamin-boehme.net>
# Licensed under GNU GPLv3; see the README and the file "COPYING" for details.

from tkGUI.view.guisettings import gui_settings
from tkinter import Toplevel, Label
from tkinter.constants import LEFT

class AboutWindow(Toplevel):
    def __init__(self):
        super().__init__()
        self.title('About')
        self._make_about_label()

    def _make_about_label(self):
        font_size_label = Label(
            self,
            justify=LEFT,
            padx=30,
            pady=30,
            font=gui_settings.fonts['default'],
            text=about_text)
        font_size_label.pack()

about_text = '''Farkle Version 0.2
Copyright (C) 2020  Benjamin Böhme  mail[at]benjamin-boehme.net

This program comes with ABSOLUTELY NO WARRANTY. This is free
software, and you are welcome to redistribute it under certain conditions.
See https://www.gnu.org/licenses/gpl-3.0 for details.'''
