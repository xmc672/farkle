# Copyright (C) 2020 Benjamin Böhme <mail[at]benjamin-boehme.net>
# Licensed under GNU GPLv3; see the README and the file "COPYING" for details.

from rulebook import raw
from tkGUI.view.guisettings import gui_settings, default_bg_color
from tkinter import Frame, Label, StringVar, Button
from tkinter.constants import LEFT, TOP, SUNKEN, RAISED, NORMAL, DISABLED, BOTH, Y

class DiceButton(Button):
    """
    View that represents a single die that can be selected or unselected
    """
    def __init__(self, master, index, pips):
        super().__init__(
            master,
            font=gui_settings.fonts['dice'],
            text=raw.ud[pips], # use unicode dice symbols
            command=lambda die_index=index: master.selection_changed_callback(die_index))

    def select(self):
        relief = SUNKEN
        bg_color = "white"
        self.config(relief=relief, bg=bg_color)

    def unselect(self):
        relief = RAISED
        self.config(relief=relief, bg=default_bg_color)

class DiceFrame(Frame):
    """
    View that holds a number of DiceButton views that can be
    - disabled (whenever the selection is not supposed to change)
    - hidden
    """
    def __init__(self, master):
        super().__init__(master)
        self.pack()
        self.dice_buttons = []
        self.selection_changed_callback = None # will be set by controller

    def _update_dice(self, dice_values, selected_indices=[]):
        self._hide_dice()
        for v, i in enumerate(dice_values):
            b = DiceButton(self, v, i)
            b.pack(side=LEFT)
            self.dice_buttons.append(b)
        for i in selected_indices:
            self.dice_buttons[i].select()

    def _disable_dice(self):
        for button in self.dice_buttons:
            button.config(state=DISABLED)

    def _hide_dice(self):
        for button in self.dice_buttons:
            button.destroy()
        self.dice_buttons = []

class ButtonFrame(Frame):
    """
    View that can show/hide/enable/disable sets of buttons:
    - action buttons (score and pass/score and continue)
    - continue buttons to acknowledge notifications etc
    """
    def __init__(self, master):
        super().__init__(master)
        self.pack()
        self.pass_button = Button(
            self,
            font=gui_settings.fonts['button'],
            text='Score and pass')
        self.reroll_button = Button(
            self,
            font=gui_settings.fonts['button'],
            text='Score and continue')
        self.continue_button = Button(
            self,
            font=gui_settings.fonts['button'],
            text='Continue')

    def show_action_buttons(self):
        self.pass_button.pack(side=LEFT)
        self.reroll_button.pack(side=LEFT)

    def hide_action_buttons(self):
        self.pass_button.pack_forget()
        self.reroll_button.pack_forget()

    def enable_action_buttons(self):
        self.pass_button.config(state=NORMAL)
        self.reroll_button.config(state=NORMAL)

    def disable_action_buttons(self):
        self.pass_button.config(state=DISABLED)
        self.reroll_button.config(state=DISABLED)

    def show_continue_button(self):
        self.continue_button.pack(side=LEFT)

    def hide_continue_button(self):
        self.continue_button.pack_forget()

    def enable_continue_button(self):
        self.continue_button.config(state=NORMAL)

    def disable_continue_button(self):
        self.continue_button.config(state=DISABLED)

class GameFrame(Frame):
    """
    View that holds:
    - Label for displaying messages about current player, state of game etc.
    - Label for displaying the accumulated turn score
    - DiceFrame
    - Label for displaying the selected score
    - ButtonFrame
    """
    def __init__(self, master):
        super().__init__(master)
        self.pack(side=LEFT)
        self.msg = Label(
            self,
            font=gui_settings.fonts['default'])
        self.msg.var = StringVar()
        self.msg.config(textvar=self.msg.var)
        self.msg.pack(side=TOP, fill=BOTH, expand=1)
        self.turn_score = Label(
            self,
            font=gui_settings.fonts['default'])
        self.turn_score.var = StringVar()
        self.turn_score.config(textvar=self.turn_score.var)
        self.turn_score.pack(side=TOP, fill=BOTH, expand=1)
        self.dice_frame = DiceFrame(self)
        self.dice_frame.pack(side=TOP, fill=Y, expand=1)
        self.selected_score = Label(
            self,
            font=gui_settings.fonts['default'])
        self.selected_score.var = StringVar()
        self.selected_score.config(textvar=self.selected_score.var)
        self.selected_score.pack(side=TOP, fill=BOTH, expand=1)
        self.button_frame = ButtonFrame(self)
        self.button_frame.pack(fill=Y, expand=1)

    def update_msg(self, msg):
        self.msg.var.set(msg)

    def show_turn_score(self):
        self.turn_score.config(textvar=self.turn_score.var)

    def hide_turn_score(self):
        self.turn_score.config(textvar=StringVar())

    def update_turn_score(self, score):
        self.turn_score.var.set('Accumulated turn score: {}'.format(score))

    def show_selected_score(self):
        self.selected_score.config(textvar=self.selected_score.var)

    def hide_selected_score(self):
        self.selected_score.config(textvar=StringVar())

    def update_selected_score(self, score, invalid=False):
        if invalid:
            self.selected_score.var.set('Invalid selection!')
        else:
            self.selected_score.var.set('Selected score: {}'.format(score))
