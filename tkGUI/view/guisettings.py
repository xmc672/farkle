# Copyright (C) 2020 Benjamin Böhme <mail[at]benjamin-boehme.net>
# Licensed under GNU GPLv3; see the README and the file "COPYING" for details.

""" Data structures for GUI settings """
from tkinter.font import Font

class GUISettings:
    def __init__(self):
        self.fonts = {
            'default': Font(name='FarkleDefaultFont', family='Helvetica'),
            'header': Font(name='FarkleHeaderFont', family='Helvetica', weight='bold'),
            'menu': Font(name='FarkleMenuFont', family='Helvetica'),
            'button': Font(name='FarkleButtonFont', family='Helvetica'),
            'small caption': Font(name='FarkleSmallCaptionFont', family='Helvetica'),
            'dice': Font(name='FarkleDiceFont', family='Helvetica'),
            'log': Font(name='FarkleLogFont', family='Monospace')
            }
        self._init_font_sizes()

    def _init_font_sizes(self):
        """ Initialize font sizes from defaults """
        for font_key, font in self.fonts.items():
            font_size = default_font_sizes[font_key]
            font.config(size=font_size)

    def adjust_font_size(self, font_key, relative_change):
        """ Set a given font's size to default size + relative change """
        try:
            font = self.fonts[font_key]
            default_size = default_font_sizes[font_key]
        except KeyError:
            raise RuntimeError('Unknown font key!')
        else:
            new_size = default_size + relative_change
            font.config(size=new_size)

    def adjust_nondice_font_sizes(self, relative_change):
        for font_key, _ in self.fonts.items():
            if font_key != 'dice':
                self.adjust_font_size(font_key, relative_change)

default_bg_color = '#d9d9d9'

default_font_sizes = {
    'default': 12,
    'header': 12,
    'menu': 10,
    'button': 10,
    'small caption': 9,
    'dice': 36,
    'log': 10
    }

gui_settings = GUISettings() # global app settings for the GUI
