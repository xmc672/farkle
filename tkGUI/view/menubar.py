# Copyright (C) 2020 Benjamin Böhme <mail[at]benjamin-boehme.net>
# Licensed under GNU GPLv3; see the README and the file "COPYING" for details.

from tkGUI.view.aboutwindow import AboutWindow
from tkGUI.view.guisettings import gui_settings
from tkGUI.view.keywindow import KeyWindow
from tkGUI.view.ruleswindow import ScrolledRulesWindow
from tkGUI.view.settingswindow import SettingsWindow
from tkinter import Menu

class MenuBar(Menu):

    def __init__(self, master):
        super().__init__(master)
        self._make_game_menu(master)
        self._make_help_menu()

    def _make_game_menu(self, master):
        """ Create pulldown menu 'Game' """
        self.game_menu = Menu(
            self,
            tearoff=0,
            font=gui_settings.fonts['menu'])
        self.game_menu.add_command(label="New game", command=None)
        self.game_menu.add_separator()
        self.game_menu.add_command(label="Settings", command=self._show_settings_window)
        self.game_menu.add_separator()
        self.game_menu.add_command(label="Exit", command=master.quit)
        self.add_cascade(label="Game", menu=self.game_menu, font=gui_settings.fonts['menu'])

    def _make_help_menu(self):
        """ Create pulldown menu 'Help' """
        help_menu = Menu(
            self,
            tearoff=0,
            font=gui_settings.fonts['menu'])
        help_menu.add_command(label="Rules", command=self._show_rules_window)
        help_menu.add_command(label="Keybindings", command=self._show_keybindings_window)
        help_menu.add_separator()
        help_menu.add_command(label="About", command=self._show_about_window)
        self.add_cascade(label="Help", menu=help_menu, font=gui_settings.fonts['menu'])

    def _show_about_window(self):
        AboutWindow()

    def _show_rules_window(self):
        ScrolledRulesWindow()

    def _show_keybindings_window(self):
        KeyWindow()

    def _show_settings_window(self):
        SettingsWindow()
