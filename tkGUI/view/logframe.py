# Copyright (C) 2020 Benjamin Böhme <mail[at]benjamin-boehme.net>
# Licensed under GNU GPLv3; see the README and the file "COPYING" for details.

from tkGUI.view.guisettings import gui_settings
from tkinter import Frame, Label
from tkinter.constants import LEFT, TOP, BOTH
from tkinter.scrolledtext import ScrolledText

class LogFrame(Frame):
    def __init__(self, master):
        super().__init__(master)
        self.pack(side=LEFT)
        self.header = Label(
            self,
            font=gui_settings.fonts['header'],
            text='Game log')
        self.header.pack(side=TOP)
        self.scrolledtext = ScrolledText(
            self,
            state='disabled',
            font=gui_settings.fonts['log'],
            bg='black',
            fg='white')
        self.scrolledtext.pack(side=TOP, fill=BOTH, expand=1)
