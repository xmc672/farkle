# Copyright (C) 2020 Benjamin Böhme <mail[at]benjamin-boehme.net>
# Licensed under GNU GPLv3; see the README and the file "COPYING" for details.

from tkGUI.view.gameframe import GameFrame
from tkGUI.view.logframe import LogFrame
from tkGUI.view.playerframe import PlayerFrame
from tkinter import PanedWindow
from tkinter.constants import BOTH, RAISED, VERTICAL, HORIZONTAL

class MainWindow(PanedWindow):
    """
    Paned view split like this:

              |  PlayerFrame
    LogFrame  |---------------
              |   GameFrame
    """
    def __init__(self):
        super().__init__(width=800, height=600, orient=HORIZONTAL)
        self.pack(fill=BOTH, expand=1)
        self.log_frame = LogFrame(self)
        self.add(self.log_frame)
        self.paneconfig(self.log_frame, minsize=300)
        self.right = PanedWindow(self, orient=VERTICAL)
        self.paneconfig(self.right, minsize=500)
        self.configure(sashrelief = RAISED)
        self.right.configure(sashrelief = RAISED)
        self.player_frame = None
        self.game_frame = None

    def reset_right_pane(self):
        self.clear_right_pane()
        self.populate_right_pane()

    def populate_right_pane(self):
        self.add(self.right)
        self.player_frame = PlayerFrame(self)
        self.right.add(self.player_frame)
        self.game_frame = GameFrame(self)
        self.right.add(self.game_frame)
        self.right.paneconfig(self.player_frame, minsize=200)

    def clear_right_pane(self):
        self.forget(self.right)
        for widget in [self.player_frame, self.game_frame]:
            if widget:
                widget.destroy()
        self.paneconfig(self.right, minsize=500)
