# Copyright (C) 2020 Benjamin Böhme <mail[at]benjamin-boehme.net>
# Licensed under GNU GPLv3; see the README and the file "COPYING" for details.

from rulebook import rulebook
from tkGUI.view.guisettings import gui_settings, default_bg_color
from tkinter import Toplevel, Listbox
from tkinter.constants import LEFT, NORMAL, SINGLE, END, BOTH, WORD, INSERT
from tkinter.scrolledtext import ScrolledText

class ScrolledRulesWindow(Toplevel):
    """
    Separate window that serves as a rulebook with a "table of contents" Listbox
    on the left hand side.
    """
    def __init__(self):
        super().__init__()
        self.title('Rules')
        self._make_listbox()
        self._make_text()

    def _make_listbox(self):
        self.listbox = Listbox(
            self,
            bg=default_bg_color,
            selectmode=SINGLE,
            font=gui_settings.fonts['menu'])
        self.listbox.pack(side=LEFT, fill=BOTH, expand=1)
        for section in rulebook.varwidth.sections:
            self.listbox.insert(END, section.title)
        self.listbox.selection_set(0)
        self.listbox.bind('<<ListboxSelect>>', lambda e: self.scroll_to_selected_section())

    def _make_text(self):
        self.text = ScrolledText(
            self,
            width=60,
            bg='black',
            fg='white',
            state=NORMAL,
            padx=40,
            pady=20,
            font=gui_settings.fonts['default'],
            wrap=WORD)
        self.text.pack(side=LEFT, fill=BOTH, expand=1)
        self.text.tag_config('b', font=gui_settings.fonts['header'])
        for section in rulebook.varwidth.sections:
            self.text.mark_set(section.title, INSERT)
            self.text.mark_gravity(section.title, direction=LEFT)
            self.text.insert(END, section.title + '\n', 'b')
            self.text.insert(END, section.body + '\n\n')
        self.text.config(state='disabled')

    def scroll_to_selected_section(self):
        try:
            section_index = self.listbox.curselection()[0]
        except IndexError:
            return
        section_mark = rulebook.varwidth.sections[section_index].title
        self.text.yview(section_mark)
