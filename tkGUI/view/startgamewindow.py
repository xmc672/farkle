# Copyright (C) 2020 Benjamin Böhme <mail[at]benjamin-boehme.net>
# Licensed under GNU GPLv3; see the README and the file "COPYING" for details.

from tkGUI.view.guisettings import gui_settings
from tkinter import Toplevel, Button, Frame, Label, Scale, Checkbutton, IntVar,\
    BooleanVar
from tkinter.constants import LEFT, HORIZONTAL

class StartGameWindow(Toplevel):
    """
    Separate window for adjusting the game settings (winning score, whether
    players are human/computer) and starting a new game.
    """
    def __init__(self):
        super().__init__()
        self.title('Start new game')
        self._make_gamesettings_frame()
        self._make_button_frame()

    def _make_gamesettings_frame(self):
        gamesettings_frame = Frame(self)
        gamesettings_frame.pack()
        gamesettings_label = Label(
            gamesettings_frame,
            font=gui_settings.fonts['menu'],
            text='Game settings')
        gamesettings_label.pack()
        self._make_winning_score_slider(gamesettings_frame)
        self._make_player_checkbuttons(gamesettings_frame)

    def _make_winning_score_slider(self, gamesettings_frame):
        self.winning_score_slider = Scale(
            gamesettings_frame,
            from_=2000,
            to=20000,
            resolution=1000,
            orient=HORIZONTAL,
            font=gui_settings.fonts['menu'],
            label='Winning score',
            command=None)
        self.winning_score_slider.var = IntVar()
        self.winning_score_slider.config(variable=self.winning_score_slider.var)
        self.winning_score_slider.pack()

    def _make_player_checkbuttons(self, gamesettings_frame):
        self.player1_checkbutton = Checkbutton(
            gamesettings_frame,
            font=gui_settings.fonts['menu'],
            text='Player1 is human')
        self.player1_checkbutton.var = BooleanVar()
        self.player1_checkbutton.config(variable=self.player1_checkbutton.var)
        self.player1_checkbutton.pack()
        self.player2_checkbutton = Checkbutton(
            gamesettings_frame,
            font=gui_settings.fonts['menu'],
            text='Player2 is human')
        self.player2_checkbutton.var = BooleanVar()
        self.player2_checkbutton.config(variable=self.player2_checkbutton.var)
        self.player2_checkbutton.pack()

    def _make_button_frame(self):
        button_frame = Frame(self)
        button_frame.pack()
        button = Button(
            button_frame,
            font=gui_settings.fonts['button'],
            text='Cancel',
            command=self.destroy)
        button.pack(side=LEFT)
        self.start_button = Button(
            button_frame,
            font=gui_settings.fonts['button'],
            text='Start',
            command=None)
        self.start_button.pack(side=LEFT)
