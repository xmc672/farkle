# Copyright (C) 2020 Benjamin Böhme <mail[at]benjamin-boehme.net>
# Licensed under GNU GPLv3; see the README and the file "COPYING" for details.

from tkGUI.view.guisettings import gui_settings
from tkinter import Frame, Label, StringVar
from tkinter.constants import LEFT, TOP

class PlayerFrame(Frame):
    """
    View with information about the Players and their bank scores.
    All players but the current one are greyed out.
    """
    def __init__(self, master):
        super().__init__(master)
        self.pack(side=LEFT)
        self.header = Label(
            self,
            font=gui_settings.fonts['header'],
            text='Bank score')
        self.header.pack(side=TOP)

    def make_player_labels(self, player_list):
        self.player_labels = []
        for p in player_list:
            l = Label(
                self,
                font=gui_settings.fonts['default'])
            l.var = StringVar()
            l.config(textvar=l.var)
            l.pack(side=TOP)
            self.player_labels.append(l)

    def update_player_labels(self, player_list, current_player, winning_score):
        if len(self.player_labels) != len(player_list):
            raise ValueError('Wrong number of entries in player_list!')
        width = len(str(winning_score))
        for i, p in enumerate(player_list):
            s = '{}: {:>{w}}/{}'.format(
                p.name,
                p.bank_score,
                winning_score,
                w=width)
            self.player_labels[i].var.set(s)
            if p == current_player:
                self.player_labels[i].config(fg='black')
            else:
                fg_color = '#777777'
                self.player_labels[i].config(fg=fg_color)
