# Copyright (C) 2020 Benjamin Böhme <mail[at]benjamin-boehme.net>
# Licensed under GNU GPLv3; see the README and the file "COPYING" for details.

from game import dice
from unittest import TestCase, main

class DieTestCase(TestCase):
    """ Tests for class Die """

    def setUp(self):
        """ Prepare tests """
        self.die = dice.Die()

    def test_init(self):
        self.assertEqual(self.die.top_face, 1)

    def test_str(self):
        self.assertEqual(str(self.die), "1")

    def test_roll(self):
        for i in range(0,10):
            self.die.roll()
            self.assertTrue(1 <= self.die.top_face <= 6)

class DiceTestCase(TestCase):
    """ Tests for class Dice """

    def setUp(self):
        """ Prepare tests """
        self.dice = dice.Dice(6)

    def test_init(self):
        self.assertEqual(self.dice.top_faces, [1]*6)
        self.assertEqual(self.dice.score, 8000)

    def test_str(self):
        self.assertEqual(str(self.dice), "[1, 1, 1, 1, 1, 1]")

    def test_roll(self):
        self.dice.roll()
        for top_face in self.dice.top_faces:
            self.assertTrue(1 <= top_face <= 6)

    def test_remove(self):
        self.dice._list[0]._top_face = 5
        self.dice._list[1]._top_face = 2
        self.dice._list[2]._top_face = 4
        self.dice._list[3]._top_face = 2
        self.dice._list[4]._top_face = 6
        self.dice._list[5]._top_face = 2
        computed_score = self.dice.remove([0, 1, 3, 5])
        expected_score = 250
        self.assertEqual(self.dice.top_faces, [4, 6])
        self.assertEqual(computed_score, expected_score)

# executable code
if __name__== '__main__':
    main()
