# Copyright (C) 2020 Benjamin Böhme <mail[at]benjamin-boehme.net>
# Licensed under GNU GPLv3; see the README and the file "COPYING" for details.

from AI.thresholdAI import ThresholdAI
from unittest import TestCase, main

class ThresholdAITestCase(TestCase):
    """ Tests for class ThresholdAI """

    def setUp(self):
        """ Prepare tests """
        self.ai = ThresholdAI(350)

    def test_get_selection_and_decision(self):
        self.assertEqual(self.ai.get_selection_and_decision([1, 2, 3, 3, 4, 6], 0), ([0], True))
        self.assertEqual(self.ai.get_selection_and_decision([1, 2, 3, 3, 4, 6], 200), ([0], True))
        self.assertEqual(self.ai.get_selection_and_decision([1, 2, 3, 3, 4, 6], 250), ([0], False))

        self.assertEqual(self.ai.get_selection_and_decision([2, 2, 3, 4, 5, 6], 0), ([4], True))
        self.assertEqual(self.ai.get_selection_and_decision([2, 2, 3, 4, 5, 6], 250), ([4], True))
        self.assertEqual(self.ai.get_selection_and_decision([2, 2, 3, 4, 5, 6], 300), ([4], False))

        self.assertEqual(self.ai.get_selection_and_decision([2, 3, 4, 4, 4, 6], 0), ([2, 3, 4], False))

        self.assertEqual(self.ai.get_selection_and_decision([1, 5, 4, 4, 4, 6], 0), ([0, 1, 2, 3, 4], False))

        self.assertEqual(self.ai.get_selection_and_decision([4, 6, 5, 2, 2, 2], 50), ([2], True))
        self.assertEqual(self.ai.get_selection_and_decision([4, 6, 5, 2, 2, 2], 100), ([2, 3, 4, 5], False))

# executable code
if __name__== '__main__':
    main()
