# Copyright (C) 2020 Benjamin Böhme <mail[at]benjamin-boehme.net>
# Licensed under GNU GPLv3; see the README and the file "COPYING" for details.

from game.dice import Die, Dice
from game.player import Player
from game.turn import Turn, TurnPhase
from unittest import TestCase, main

class TurnTestCase(TestCase):
    """ Tests for class Turn """

    def setUp(self):
        """ Prepare tests """
        self.player = Player(1, 'Player 1')
        self.turn = Turn(self.player)

    def test_init(self):
        self.assertFalse(self.turn.is_over)
        self.assertEqual(self.turn.phase, TurnPhase.ROLL)
        self.assertEqual(self.turn.score, 0)
        self.assertEqual(self.turn.roll_score, 0)
        self.assertEqual(self.turn.current_player, self.player)

    def test_roll_dice__wrong_phase_exception(self):
        self.turn._phase = TurnPhase.BANK
        self.assertRaises(RuntimeError, self.turn.roll_dice)
        self.turn._phase = TurnPhase.FARKLE
        self.assertRaises(RuntimeError, self.turn.roll_dice)
        self.turn._phase = TurnPhase.SELECT
        self.assertRaises(RuntimeError, self.turn.roll_dice)

    def test_roll_dice__make_new_dice(self):
        self.turn._dice = Dice(0)
        self.turn.roll_dice()
        self.assertEqual(len(self.turn.dice_values), 6)

    def test_roll_dice__roll_and_update(self):
        self.turn.roll_dice()
        self.assertNotEqual(self.turn.dice_values, [1, 1, 1, 1, 1, 1], 'Might fail by accident, retry!')
        self.assertIn(self.turn.phase, [TurnPhase.FARKLE, TurnPhase.SELECT])

    def test_score_dice__wrong_phase_exception(self):
        self.turn._phase = TurnPhase.BANK
        self.assertRaises(RuntimeError, self.turn.score_dice, [])
        self.turn._phase = TurnPhase.FARKLE
        self.assertRaises(RuntimeError, self.turn.score_dice, [])
        self.turn._phase = TurnPhase.ROLL
        self.assertRaises(RuntimeError, self.turn.score_dice, [])

    def test_score_dice__validation(self):
        self.turn._phase = TurnPhase.SELECT
        self.assertRaises(ValueError, self.turn.score_dice, [])
        test_die = Die()
        test_die._top_face = 2
        test_dice = Dice(3)
        for i in range(3):
            test_dice._list[i] = test_die
        self.turn._dice = test_dice
        self.assertRaises(ValueError, self.turn.score_dice, [0, 1])

    def test_score_dice__score_and_update(self):
        self.turn._phase = TurnPhase.SELECT
        die2 = Die()
        die2._top_face = 2
        die5 = Die()
        die5._top_face = 5
        die6 = Die()
        die6._top_face = 6
        self.turn._dice._list = [die2, die2, die5, die6, die2, die2]
        self.turn._score = 123
        self.turn.score_dice([0, 1, 2, 4, 5])
        self.assertEqual(self.turn.roll_score, 450)
        self.assertEqual(self.turn.score, 573)
        self.assertEqual(self.turn.phase, TurnPhase.ROLL)

    def test_start_banking_phase(self):
        self.assertEqual(self.turn.phase, TurnPhase.ROLL)
        self.turn.start_banking_phase()
        self.assertEqual(self.turn.phase, TurnPhase.BANK)

# executable code
if __name__== '__main__':
    main()
