# Copyright (C) 2020 Benjamin Böhme <mail[at]benjamin-boehme.net>
# Licensed under GNU GPLv3; see the README and the file "COPYING" for details.

from game.player import Player
from unittest import TestCase, main

class PlayerTestCase(TestCase):
    """ Tests for class Player """

    def setUp(self):
        """ Prepare tests """
        self.player = Player(5, 'Jane Doe')

    def test_init(self):
        self.assertEqual(self.player.id, 5)
        self.assertEqual(self.player.name, 'Jane Doe')
        self.assertEqual(self.player.is_human, True)
        self.assertEqual(self.player.bank_score, 0)

    def test_increase_bank_score(self):
        self.player.increase_bank_score(50)
        self.assertEqual(self.player.bank_score, 50)
        self.player.increase_bank_score(100)
        self.assertEqual(self.player.bank_score, 150)

# executable code
if __name__== '__main__':
    main()
