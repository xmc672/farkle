# Copyright (C) 2020 Benjamin Böhme <mail[at]benjamin-boehme.net>
# Licensed under GNU GPLv3; see the README and the file "COPYING" for details.

from game import eval
from unittest import TestCase, main

class EvalTestCase(TestCase):
    """ Tests for module eval """

    def setUp(self):
        """ Prepare tests """
        self._setUp_dice_rolls()

    def _setUp_dice_rolls(self):
        self.dice_rolls = []
        self.dice_rolls.append(DiceAndScore(MockDice([2, 2, 3, 3, 4, 6]), 0))
        self.dice_rolls.append(DiceAndScore(MockDice([1, 2, 3, 3, 4, 6]), 100))
        self.dice_rolls.append(DiceAndScore(MockDice([2, 2, 3, 4, 5, 6]), 50))
        self.dice_rolls.append(DiceAndScore(MockDice([2, 2, 2, 3, 4, 6]), 200))
        self.dice_rolls.append(DiceAndScore(MockDice([2, 3, 4, 4, 4, 6]), 400))
        self.dice_rolls.append(DiceAndScore(MockDice([1, 4, 4, 4, 5, 6]), 550))
        self.dice_rolls.append(DiceAndScore(MockDice([2, 5, 5, 5, 5, 6]), 1000))
        self.dice_rolls.append(DiceAndScore(MockDice([1, 1, 1, 1, 1, 6]), 4000))

    def test_find_pips(self):
        dice_values = [2, 2, 3, 3, 4, 6]
        self.assertEqual(eval.find_pips(1, dice_values), [])
        self.assertEqual(eval.find_pips(2, dice_values), [0, 1])
        self.assertEqual(eval.find_pips(3, dice_values), [2, 3])
        self.assertEqual(eval.find_pips(4, dice_values), [4])
        self.assertEqual(eval.find_pips(5, dice_values), [])
        self.assertEqual(eval.find_pips(6, dice_values), [5])

    def test_find_all_scoring_dice(self):
        roll_values = [dr.dice.top_faces for dr in self.dice_rolls]
        # compare sets because find_all_scoring_dice doesn't have ordered output
        self.assertEqual(set(eval.find_all_scoring_dice(roll_values[0])), set())
        self.assertEqual(set(eval.find_all_scoring_dice(roll_values[1])), {0})
        self.assertEqual(set(eval.find_all_scoring_dice(roll_values[2])), {4})
        self.assertEqual(set(eval.find_all_scoring_dice(roll_values[3])), {0, 1, 2})
        self.assertEqual(set(eval.find_all_scoring_dice(roll_values[4])), {2, 3, 4})
        self.assertEqual(set(eval.find_all_scoring_dice(roll_values[5])), {0, 1, 2, 3, 4})
        self.assertEqual(set(eval.find_all_scoring_dice(roll_values[6])), {1, 2, 3, 4})
        self.assertEqual(set(eval.find_all_scoring_dice(roll_values[7])), {0, 1, 2, 3, 4})

    def test_find_three_of_a_kind(self):
        self.assertEqual(eval.find_three_of_a_kind([2, 5, 3, 5, 4, 5]), [1, 3, 5])
        self.assertEqual(eval.find_three_of_a_kind([3, 3, 3, 4, 4, 4]), [3, 4, 5])
        self.assertRaises(ValueError, eval.find_three_of_a_kind, [1, 2, 3, 4, 5, 6])

    def test_is_valid_selection(self):
        self.assertFalse(eval.is_valid_selection([]))
        self.assertFalse(eval.is_valid_selection([2, 2, 3, 4, 6, 6]))
        self.assertFalse(eval.is_valid_selection([1, 2, 3, 4, 6, 6]))
        self.assertTrue(eval.is_valid_selection([2, 2, 2, 5]))
        self.assertTrue(eval.is_valid_selection([1, 5]))

    def test_is_farkle(self):
        for dice_roll in self.dice_rolls:
            dice = dice_roll.dice
            self.assertEqual(eval.is_farkle(dice), dice_roll.score == 0)

    def test_get_score_from_dice(self):
        for dice_roll in self.dice_rolls:
            dice = dice_roll.dice
            computed_score = eval.get_score_from_dice(dice)
            expected_score = dice_roll.score
            self.assertEqual(computed_score, expected_score)

    def test_get_score_from_values(self):
        for dice_roll in self.dice_rolls:
            values = dice_roll.dice.top_faces
            computed_score = eval.get_score_from_values(values)
            expected_score = dice_roll.score
            self.assertEqual(computed_score, expected_score)

class MockDice():
    def __init__(self, top_faces):
        self._top_faces = top_faces

    @property
    def top_faces(self):
        return self._top_faces

class DiceAndScore:
    def __init__(self, dice, score):
        self.dice = dice
        self.score = score

# executable code
if __name__== '__main__':
    main()
