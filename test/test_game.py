# Copyright (C) 2020 Benjamin Böhme <mail[at]benjamin-boehme.net>
# Licensed under GNU GPLv3; see the README and the file "COPYING" for details.

from game.game import Game, GameSettings
from unittest import TestCase, main

class GameTestCase(TestCase):
    """ Tests for class Game """

    def setUp(self):
        """ Prepare tests """
        settings = GameSettings()
        self.game = Game(settings)

    def test_init(self):
        self.assertEqual(self.game.winning_score, 6000)
        self.assertFalse(self.game.is_over)
        self.assertFalse(self.game.is_last_round)
        current_player = self.game.current_player
        self.assertEqual(current_player.id, 1)
        self.assertEqual(current_player.name, 'Player 1')
        self.assertEqual(current_player.bank_score, 0)
        self.assertEqual(self.game.winner, None)

    def test_update_game_over_exception(self):
        self.game._is_over = True
        self.assertRaises(RuntimeError, self.game.update)

    def test_update_turn_over_exception(self):
        self.game._turn._is_over = False
        self.assertRaises(RuntimeError, self.game.update)

    def test_update_trigger_last_round(self):
        win_score = self.game.winning_score
        self.game.current_player.increase_bank_score(win_score)
        self.game.turn._is_over = True # avoid exception
        self.game.update()
        self.assertTrue(self.game.is_last_round)

    def test_update_end_game(self):
        self.game.turn._is_over = True # avoid exception
        self.game._is_last_round = True
        number_of_players = len(self.game.player_list)
        self.game._players._index = number_of_players - 1
        self.game.update()
        self.assertTrue(self.game.winner)
        self.assertTrue(self.game.is_over)

    def test_update_make_next_turn(self):
        self.game.turn._is_over = True # avoid exception
        self.assertFalse(
            self.game.is_last_round,
            'If this fails, there\' is a problem with initializing is_last_round!')
        self.game.update()
        current_player_index = self.game.current_player.id
        self.assertEqual(current_player_index, 2)
        self.assertFalse(self.game.is_over)

    def test_update_make_final_turn(self):
        self.game.turn._is_over = True # avoid exception
        self.game._is_last_round = True
        self.assertFalse(
            self.game._players.at_end_of_loop,
            'If this fails, there is a problem with initializing the PlayerLoop!')
        self.game.update()
        current_player_index = self.game.current_player.id
        self.assertEqual(current_player_index, 2)
        self.assertFalse(self.game.is_over)

# executable code
if __name__== '__main__':
    main()
