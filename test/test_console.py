# Copyright (C) 2020 Benjamin Böhme <mail[at]benjamin-boehme.net>
# Licensed under GNU GPLv3; see the README and the file "COPYING" for details.

from simpleUI import console
from unittest import TestCase, main

class TurnTestCase(TestCase):
    """ Tests for module 'console' """

    def setUp(self):
        """ Prepare tests """
        pass

    def test__string_to_index_list__not_only_digits(self):
        self.assertRaises(ValueError, console._string_to_index_list, '1a2b', 5)

    def test__string_to_index_list__exceeds_max_index(self):
        self.assertRaises(ValueError, console._string_to_index_list, '126', 5)

    def test__string_to_index_list__duplicate_entries(self):
        self.assertRaises(ValueError, console._string_to_index_list, '112', 5)

    def test__string_to_index_list__return_value(self):
        self.assertEqual(console._string_to_index_list('3451', 5), [2, 3, 4, 0])

# executable code
if __name__== '__main__':
    main()
