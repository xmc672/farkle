# Copyright (C) 2020 Benjamin Böhme <mail[at]benjamin-boehme.net>
# Licensed under GNU GPLv3; see the README and the file "COPYING" for details.

from game.playerloop import PlayerLoop
from unittest import TestCase, main

class PlayerLoopTestCase(TestCase):
    """ Tests for class PlayerLoop """

    def setUp(self):
        """ Prepare tests """
        self.loop = PlayerLoop([True, True, False, False])

    def test_init(self):
        self.assertEqual(self.loop._index, 0)
        self.assertEqual(self.loop._player_list[0].id, 1)
        self.assertEqual(self.loop._player_list[0].name, 'Player 1')
        self.assertEqual(self.loop._player_list[0].is_human, True)
        self.assertEqual(self.loop._player_list[3].id, 4)
        self.assertEqual(self.loop._player_list[3].name, 'Player 4')
        self.assertEqual(self.loop._player_list[3].is_human, False)

    def test_current_player(self):
        self.assertEqual(self.loop.current_player.id, 1)
        self.loop.loop()
        self.assertEqual(self.loop.current_player.id, 2)
        self.loop.loop()
        self.assertEqual(self.loop.current_player.id, 3)
        self.loop.loop()
        self.assertEqual(self.loop.current_player.id, 4)

    def test_highest_scoring_player(self):
        self.loop.current_player.increase_bank_score(50)
        self.loop.loop()
        self.loop.current_player.increase_bank_score(400)
        player2 = self.loop.current_player
        self.loop.loop()
        self.loop.current_player.increase_bank_score(200)
        self.loop.loop()
        self.loop.current_player.increase_bank_score(100)
        self.assertEqual(self.loop.highest_scoring_player, player2)

    def test_at_end_of_loop(self):
        self.assertFalse(self.loop.at_end_of_loop)
        self.loop.loop()
        self.assertFalse(self.loop.at_end_of_loop)
        self.loop.loop()
        self.assertFalse(self.loop.at_end_of_loop)
        self.loop.loop()
        self.assertTrue(self.loop.at_end_of_loop)

    def test_list_players(self):
        list = self.loop.list_players()
        for i in range(1, 5):
            self.assertEqual(list[i-1].id, i)
            self.assertEqual(list[i-1].name, 'Player {}'.format(i))

# executable code
if __name__== '__main__':
    main()
